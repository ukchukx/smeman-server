<?php

namespace App\Api\V1\Controllers;

use JWTAuth;
use Validator;
use Config;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Mail\Message;
use Dingo\Api\Routing\Helpers;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\DB;
use Tymon\JWTAuth\Exceptions\JWTException;
use Dingo\Api\Exception\ValidationHttpException;
use Carbon\Carbon;

class AuthController extends Controller {

	use Helpers;

	public function login(Request $request) {
		$credentials = $request->only(['email', 'password']);

		$validator = Validator::make($credentials, [
					'email' => 'required',
					'password' => 'required',
		]);



		if ($validator->fails()) {
			throw new ValidationHttpException($validator->errors()->all());
		}

		try {
			if (!$token = JWTAuth::attempt($credentials)) {
				return $this->response->errorUnauthorized();
			}
		} catch (JWTException $e) {
			return $this->response->error('could_not_create_token', 500);
		}

		$user = User::where('email', $credentials['email'])->first();
		$id = $user->id;

		return response()->json(compact('token', 'id'));
	}

	public function refresh(Request $request) {
		$token = JWTAuth::getToken();

		if (!$token) {
			// Help JWTAuth find the token
			$token = $request->header('authorization');
			if (!$token)
				return $this->response->errorBadRequest('Token not provided');

			$token_parts = explode(' ', $token);
			if (count($token_parts) != 2)
				return $this->response->errorBadRequest('Token not properly formed');
			$token = $token_parts[1];
		}

		try {
			$token = JWTAuth::refresh($token);
		} catch (TokenInvalidException $e) {
			return $this->response->errorForbidden('The token is invalid');
		}

		return response()->json(compact('token'));
	}

	public function signup(Request $request) {
		$signupFields = Config::get('boilerplate.signup_fields');
		$hasToReleaseToken = Config::get('boilerplate.signup_token_release');

		$userData = $request->only($signupFields);

		$validator = Validator::make($userData, Config::get('boilerplate.signup_fields_rules'));

		if ($validator->fails()) {
			throw new ValidationHttpException($validator->errors()->all());
		}

		User::unguard();
		$user = User::create($userData);
		User::reguard();

		if (!$user->id) {
			return $this->response->error('could_not_create_user', 500);
		}

		if ($hasToReleaseToken) {
			return $this->login($request);
		}

		return $this->response->created();
	}

	public function recovery(Request $request) {
		$validator = Validator::make($request->only('email'), [
					'email' => 'required'
		]);

		if ($validator->fails()) {
			throw new ValidationHttpException($validator->errors()->all());
		}

		$result = $this->getResetToken($request);
		if ($result == 'not_found')
			return $this->response->errorNotFound();
		$token = $result;

		$route = url('/0xx0/reset', ['token' => $result]);
		$content = "Use this link to reset your password: {$route}";

		$user = Password::getUser($request->only('email'));
		$p = [
			'title' => 'Password recovery',
			'to' => $user->email,
			'content' => $content
		];

		$result = $this->sendMail($p);
		if ($result)
			return response()->json(['route' => $route, 'token' => $token]);
		else
			return $this->response->errorInternal('Could not send mail');
	}

	public function reset(Request $request) {
		$credentials = $request->only(
				'email', 'password', 'password_confirmation', 'token'
		);

		$validator = Validator::make($credentials, [
					'token' => 'required',
					'email' => 'required|email',
					'password' => 'required|confirmed|min:6',
		]);

		if ($validator->fails()) {
			throw new ValidationHttpException($validator->errors()->all());
		}

		$response = Password::reset($credentials, function ($user, $password) {
					$user->password = $password;
					$user->save();
				});

		switch ($response) {
			case Password::PASSWORD_RESET:
				if (Config::get('boilerplate.reset_token_release')) {
					return $this->login($request);
				}
				return $this->response->noContent();

			default:
				return $this->response->error('could_not_reset_password', 500);
		}
	}

	private function getResetToken(Request $request) {
		$user = Password::getUser($request->only('email'));
		if (is_null($user)) {
			return 'not_found';
		}
		$email = $user->email;
		DB::table('password_resets')->where('email', $user->email)->delete();
		$token = Password::getRepository()->createNewToken();
		DB::table('password_resets')
				->insert(['email' => $email, 'token' => $token, 'created_at' => new Carbon]);

		return $token;
	}

}
