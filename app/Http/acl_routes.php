<?php

$api = app('Dingo\Api\Routing\Router');

$api->version('v1', function ($api) {
  $api->group(['middleware' => ['api.auth', 'jwt.refresh']], function ($api) {
    $api->post('acl/role', 'App\Api\V1\Controllers\ACLController@createRole');
  	$api->post('acl/permission', 'App\Api\V1\Controllers\ACLController@createPermission');
  	$api->post('acl/assign-role', 'App\Api\V1\Controllers\ACLController@assignRole');
  	$api->post('acl/attach-permission', 'App\Api\V1\Controllers\ACLController@attachPermission');
	});
});
