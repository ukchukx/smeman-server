<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
      'first_name' => $faker->firstName,
      'other_name' => $faker->firstName,
      'last_name' => $faker->lastName,
      'email' => $faker->freeEmail,
      'password' => $faker->password,
      'phone' => $faker->e164PhoneNumber,
      'password' => \Hash::make(str_random(10))
    ];
});

$factory->defineAs(App\Role::class, 'SuperAdmin', function () {
    return [
      'name' => 'SuperAdmin',
      'description' => 'Super Admin',
      'display_name' => 'Super Admin'
    ];
});

$factory->defineAs(App\Role::class, 'Admin', function () {
    return [
      'name' => 'Admin',
      'description' => 'Admin',
      'display_name' => 'Admin'
    ];
});
$factory->defineAs(App\Role::class, 'User', function () {
    return [
      'name' => 'User',
      'description' => 'User',
      'display_name' => 'User'
    ];
});
