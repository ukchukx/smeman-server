<?php

class TestCase extends Illuminate\Foundation\Testing\TestCase
{
    /**
     * The base URL to use while testing the application.
     *
     * @var string
     */
    protected $baseUrl = 'http://localhost';

    /**
     * Creates the application.
     *
     * @return \Illuminate\Foundation\Application
     */
    public function createApplication()
    {
        $app = require __DIR__.'/../bootstrap/app.php';

        $app->make(Illuminate\Contracts\Console\Kernel::class)->bootstrap();

        return $app;
    }

    /**
  * See if the response has a header.
  *
  * @param $header
  * @return $this
  */
  public function seeHasHeader($header) {
    $this->assertTrue(
      $this->response->headers->has($header),
      "Response should have the header '{$header}' but does not."
    );

    return $this;
  }

  /**
  * Asserts that the response header matches a given regular expression.
  * @param $header
  * @param $regexp
  * @return $this
  */
  public function seeHeaderWithRegExp($header, $regexp) {
    $this
      ->seeHasHeader($header)
      ->assertRegExp(
        $regexp,
        $this->response->headers->get($header)
      );

    return $this;
  }
}
