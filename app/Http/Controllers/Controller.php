<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;
use Dingo\Api\Exception\ValidationHttpException;
use Illuminate\Http\Request;
use JWTAuth;
use Validator;
use Config;
use Mail;

class Controller extends BaseController {
  use AuthorizesRequests, AuthorizesResources, DispatchesJobs, ValidatesRequests;
  // protected $validation_rules = [
  //   'email' => 'required|email|unique:users',
  //   'first_name' => 'required|string|max:30|min:2',
  //   'last_name' => 'required|string|max:30|min:2',
  //   'password' => 'max:100|min:6'
  // ];
  //
  // protected $validation_error_msgs = [
  //   'password.min' => 'Passwords must be at least :min characters long',
  //   'password.max' => 'Passwords must be at most :max characters long',
  //   'required' => ':attribute is required',
  //   'min' => 'Min. length for :attribute is 2',
  //   'max' => 'Max. length for :attribute is 30',
  //   'unique' => ':attribute is already in use'
  // ];

  protected function getAndValidateUserData($request) {
    $data = $request->all();

    $validator = Validator::make($data, Config::get('boilerplate.signup_fields_rules'));

    if($validator->fails()) {
      throw new ValidationHttpException($validator->errors()->all());
    }

    return $data;
  }

  /**
  * Sends mail.
  * At the very least, there should be title, content & to should be provided
  */
  protected function sendMail($data = []) {
    if (count($data) < 3) {
      return 'insufficient_params';
    }
    return Mail::send('emails.send',
      ['title' => $data['title'], 'content' => $data['content']], function ($message) use ($data) {
        $message->from(trim(Config::get('boilerplate.recovery_email_subject')), 'Email bot');
        $message->to(trim($data['to']));
    });
  }

  protected function getRequestingUser(Request $request = null) {
    return JWTAuth::parseToken()->authenticate();
    // $header = 'authorization';
    // $method = 'bearer';
    // $header = $request->headers->get($header);
    //
    // if (! starts_with(strtolower($header), $method)) {
    //     return false;
    // }
    // return JWTAuth::setToken(trim(str_ireplace($method, '', $header)))->authenticate();
  }
}
