<?php

namespace App\Api\V1\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Api\V1\Transformers\UserTransformer;
use Dingo\Api\Routing\Helpers;
use App\Http\Requests;
use Dingo\Api\Exception\ValidationHttpException;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class UserController extends Controller {
  use Helpers;

  public function index() {
    $requestingUser = $this->getRequestingUser();

    return $this->response->collection(User::all(), new UserTransformer);
  }

  public function show($id) {
    $requestingUser = $this->getRequestingUser();
    try {
      $user = User::findOrFail($id);
    } catch (ModelNotFoundException $e) {
      return $this->response->errorNotFound();
    }

    // Normal users can only view their own record
    if ($requestingUser->hasRole('User') && $user->id != $requestingUser->id) {
      return $this->response->errorForbidden();
    }

    return $this->response->item($user, new UserTransformer);
  }

  public function store(Request $request) {
    $requestingUser = $this->getRequestingUser();

    $userData = $this->getAndValidateUserData($request);

    User::unguard();
    $user = User::create($userData);
    User::reguard();

    if(!$user->id) {
        return $this->response->error('could_not_create_user', 500);
    }

    return $this->response->created(
      app('Dingo\Api\Routing\UrlGenerator')->version('v1')->route('users.get', ['id' => $user->id])
    );
  }

  public function update(Request $request, $id) {
    $requestingUser = $this->getRequestingUser();

    try {
      $user = User::findOrFail($id);
    } catch (ModelNotFoundException $e) {
      return $this->response->errorNotFound();
    }

    // Normal users can only view their own record
    if ($requestingUser->hasRole('User') && $user->id != $requestingUser->id) {
      return $this->response->errorForbidden();
    }

    $userData = $this->getAndValidateUserData($request);

    User::unguard();
    $user->fill($userData);
    User::reguard();
    $user->save();
    return $this->response()->item($user, new UserTransformer);
  }

  public function destroy($id) {
    $requestingUser = $this->getRequestingUser();

    try {
      User::findOrFail($id)->delete();
    } catch (ModelNotFoundException $e) {
      return $this->response->errorNotFound();
    }

    return $this->response->noContent();
  }

}
