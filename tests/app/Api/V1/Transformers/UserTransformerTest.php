<?php
namespace Tests\App\Api\V1\Transformer;

use TestCase;
use App\User;
use App\Api\V1\Transformers\UserTransformer;
use League\Fractal\TransformerAbstract;
use Illuminate\Foundation\Testing\DatabaseMigrations;


class UserTransformerTest extends TestCase {
  use DatabaseMigrations;

  /** @test **/
  public function canBeInitialized() {
    $subject = new UserTransformer;
    $this->assertInstanceOf(TransformerAbstract::class, $subject);
  }

  /** @test **/
  public function itTransformsAUserModel() {
    $user = factory(User::class)->create();
    $subject = new UserTransformer;
    $transform = $subject->transform($user);

    $this->assertArrayHasKey('id', $transform);
    $this->assertArrayHasKey('email', $transform);
    $this->assertArrayHasKey('created_at', $transform);
    $this->assertArrayHasKey('updated_at', $transform);
    $this->assertArrayHasKey('first_name', $transform);
    $this->assertArrayHasKey('last_name', $transform);
    $this->assertArrayHasKey('user_type', $transform);
    $this->assertArrayHasKey('phone', $transform);
    $this->assertArrayNotHasKey('password', $transform);
  }
}
