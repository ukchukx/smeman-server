<?php
namespace Tests\App\Api\V1\Controllers;

use TestCase;
use App\User;
use App\Role;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Carbon\Carbon;

class UserControllerTest extends TestCase {
  use DatabaseMigrations;

  public function setUp() {
    parent::setUp();

    $this->users_endpoint = '/api/users';
    $this->users_db = 'users';

    $auth_user = factory('App\User')->create(['password' => 'test_test']);

    $role = Role::where('name', 'Admin')->first();
    if (!isset($role)) $role = factory('App\Role', 'Admin')->create();
    $auth_user->attachRole($role);

    // Get token
    $this->post('/api/auth/login',
      ['email' => $auth_user->email, 'password' => 'test_test']
    );

    $data = json_decode($this->response->getContent(), true);
    $this->token = $data['token'];
    $this->auth_header =  ['HTTP_Authorization' => 'Bearer ' . $this->token];
  }

  private function makeAUser($args = []) {
    return factory('App\User')->make($args)->toArray();
  }

  /** @test **/
  public function getAllUsersShouldBe200() {
    $this->get($this->users_endpoint, $this->auth_header)->seeStatusCode(200);
  }

  /** @test **/
  public function getAllUsersShouldReturnACollectionOfRecords() {
    $users = factory('App\User', 2)->create();

    $this->get($this->users_endpoint, $this->auth_header);

    foreach($users as $user) {
      $this->seeJson($user->toArray());
    }
  }

  /** @test **/
  public function getShouldReturnAValidUser() {
    $user = factory('App\User')->create();

    $this
      ->get($this->users_endpoint."/{$user->id}", $this->auth_header)
      ->seeStatusCode(200)
      ->seeJson($user->toArray());

    $data = json_decode($this->response->getContent(), true);
    $data = $data['data'];
    $this->assertArrayHasKey('created_at', $data);
    $this->assertArrayHasKey('updated_at', $data);
    $this->assertEquals($user->email, $data['email']);
  }

  /** @test **/
  public function getShouldFailWithoutAValidUserId() {
    $this
      ->get($this->users_endpoint.'/111111', $this->auth_header)
      ->seeStatusCode(404)
      ->seeJson([
        'message' => 'Not Found',
        'status_code' => 404
      ]);
  }

  /** @test **/
  public function getShouldNotMatchAnInvalidRoute() {
    $this->get($this->users_endpoint.'/bad-id', $this->auth_header)->seeStatusCode(404);
  }

  /** @test **/
  public function postShouldSaveUserInDB() {
    $user = $this->makeAUser();
    $this
      ->post($this->users_endpoint, $user, $this->auth_header)
      ->seeInDatabase($this->users_db, ['email' => $user['email']]);
  }

  /** @test **/
  public function postShouldReturn201AndLocationHeader() {
    $this
      ->post($this->users_endpoint, $this->makeAUser(), $this->auth_header)
      ->seeStatusCode(201)
      ->seeHeaderWithRegExp('Location', '#'.$this->users_endpoint.'/[\d]+$#');
  }

  /** @test **/
  public function updateShouldOnlyChangeRelevantFields() {
    $user = factory('App\User')->create([
      'id' => 300,
      'last_name' => 'Chukundah',
      'first_name' => 'Ukay',
      'password' => 'asjdfkl324sf',
      'phone' => '08056674343',
      'email' => 'test@yahoo.com'
    ]);

    $now = Carbon::now();

    $new_data = [
      'id' => 300,
      'last_name' => 'Chukx',
      'first_name' => 'Uk',
      'password' => 'asjdfkl32234sf',
      'created_at' => $now->__toString(),
      'updated_at' => $now->__toString(),
      'phone' => '08056674343',
      'email' => 'uk@yahoo.com'
    ];

    $this->put($this->users_endpoint."/{$user->id}", $new_data, $this->auth_header);

    $this
      ->seeStatusCode(200)
      ->seeJson([
        'id' => $user->id,
        'last_name' => $new_data['last_name'],
        'first_name' => $new_data['first_name'],
        'phone' => $user->phone,
        'email' => $new_data['email']
      ])
      ->seeInDatabase($this->users_db, ['email' => $new_data['email']]);

    $data = json_decode($this->response->getContent(), true);
    $data = $data['data'];
    $last_updated = Carbon::createFromFormat('Y-m-d H:i:s', $data['updated_at']);
    $this->assertEquals($data['created_at'], $user->created_at);
    $this->assertFalse($last_updated->lt($now), "{$last_updated} is less than {$now}");

  }

  /** @test **/
  public function updateShouldFailWithInvalidID() {
    $this
     ->put($this->users_endpoint.'/9999999', [], $this->auth_header)
     ->seeStatusCode(404)
     ->seeJson([
       'message' => 'Not Found',
       'status_code' => 404
     ]);
  }

  /** @test **/
  /** @test **/
  public function updateShouldNotMatchInvalidRoute() {
    $this
     ->put($this->users_endpoint.'/not-allowed', [], $this->auth_header)
     ->seeStatusCode(404);
  }

  /** @test **/
  public function deleteShouldRemoveAValidUser() {
    $user = factory('App\User')->create();

    $this
      ->delete($this->users_endpoint."/{$user->id}", [], $this->auth_header)
      ->seeStatusCode(204)
      ->isEmpty();

    $this->notSeeInDatabase($this->users_db, ['id' => $user->id]);
  }

  /** @test **/
  public function deleteShouldFailWithInvalidID() {
    $this
     ->delete($this->users_endpoint.'/9999999', [], $this->auth_header)
     ->seeStatusCode(404)
     ->seeJson([
       'message' => 'Not Found',
       'status_code' => 404
     ]);
  }

  /** @test **/
  public function deleteShouldNotMatchInvalidRoute() {
    $this
      ->delete($this->users_endpoint.'/not-allowed', [], $this->auth_header)
      ->seeStatusCode(404);
  }

}
