<?php

namespace App\Api\V1\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use JWTAuth;
use App\User;
use App\Permission;
use App\Role;
use Config;
use Validator;
use App\Api\V1\Transformers\UserTransformer;
use Dingo\Api\Routing\Helpers;
use App\Http\Requests;
use Dingo\Api\Exception\ValidationHttpException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Log;

class ACLController extends Controller {
  public function createRole(Request $request) {
    $requestingUser = $this->getRequestingUser();

    if (!($requestingUser->hasRole('SuperAdmin') || $requestingUser->can('create-role'))) {
      return $this->response->errorForbidden();
    }

    $role = new Role;
    $role->name = ucfirst($request->input('name'));
    $role->description = $request->input('description', $role->name);
    $role->display_name = $request->input('display_name', $role->name);
    $role->save();
    return $this->response->created();
  }

  public function createPermission(Request $request) {
    $requestingUser = $this->getRequestingUser();

    if (!($requestingUser->hasRole('SuperAdmin') || $requestingUser->can('create-permission'))) {
      return $this->response->errorForbidden();
    }
    $perm = new Permission;
    $perm->name = strtolower($request->input('name'));
    $perm->description = $perm->name;
    $perm->display_name = $request->input('display_name', $perm->name);
    $perm->save();
    return $this->response->created();
  }

  public function assignRole(Request $request) {
    $requestingUser = $this->getRequestingUser();

    if (!($requestingUser->hasRole('SuperAdmin') || $requestingUser->can('assign-role'))) {
      return $this->response->errorForbidden();
    }

    $user = User::find($request->input('user_id'));
    $role = Role::where('name', $request->input('role'))->first();

    $user->roles()->attach($role->id);

    return $this->response->created();
  }

  public function attachPermission(Request $request) {
    $requestingUser = $this->getRequestingUser();

    if (!($requestingUser->hasRole('SuperAdmin') || $requestingUser->can('attach-permission'))) {
      return $this->response->errorForbidden();
    }
    $role = Role::where('name', $request->input('role'))->first();
    $permission = Permission::where('name', $request->input('name'))->first();
    $role->attachPermission($permission);

    return $this->response->created();
  }
}
