<?php

$api = app('Dingo\Api\Routing\Router');

$api->version('v1', function ($api) {
	$api->users_endpoint = 'users';

	$api->group(['middleware' => ['api.auth', 'jwt.refresh']], function ($api) {
		$api->get($api->users_endpoint.'/{id}', [
			'middleware' => ['ability:SuperAdmin|Admin|User,permission:view-self|view-users'],
			'as' => 'users.get',
			'uses' => 'App\Api\V1\Controllers\UserController@show'
		])->where('id', '[0-9]+');
		$api->get($api->users_endpoint, [
			'middleware' => ['ability:SuperAdmin|Admin,permission:create-users'],
			'as' => 'users.index',
			'uses' => 'App\Api\V1\Controllers\UserController@index'
		]);
		$api->post($api->users_endpoint, [
			'middleware' => ['ability:SuperAdmin|Admin,permission:create-users'],
			'as' => 'users.create',
			'uses' => 'App\Api\V1\Controllers\UserController@store'
		]);
		$api->put($api->users_endpoint.'/{id}', [
			'middleware' => ['ability:SuperAdmin|Admin,permission:create-users|update-users|update-self'],
			'as' => 'users.update',
		 	'uses' => 'App\Api\V1\Controllers\UserController@update'
		])->where('id', '[0-9]+');
		$api->delete($api->users_endpoint.'/{id}', [
			'middleware' => ['ability:SuperAdmin|Admin,permission:create-users|delete-users'],
			'as' => 'users.delete',
			'uses' => 'App\Api\V1\Controllers\UserController@destroy'
		])->where('id', '[0-9]+');
	});


	// example of protected route
	// $api->get('protected', ['middleware' => ['api.auth'], function () {
	// 	return \App\User::all();
  //   }]);
	//
	// // example of free route
	// $api->get('free', function() {
	// 	return \App\User::all();
	// });

});
