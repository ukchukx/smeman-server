<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class UsersTableSeeder extends Seeder {
  private $table = 'users';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
      Model::unguard();
      DB::table($this->table)->delete();
      $now = Carbon::now();
      $users = [
        [
          'email' => 'popsyjunior@yahoo.com',
          'first_name' => 'Popsana',
          'last_name' => 'Barida',
          'other_name' => 'Noble',
          'phone' => '08065589845',
          'password' => \Hash::make('popsana'),
          'created_at' => $now,
          'updated_at' => $now
        ],
        [
          'email' => 'uk_chukundah@yahoo.com',
          'first_name' => 'Uk',
          'last_name' => 'Chukundah',
          'phone' => '08125390122',
          'password' => \Hash::make('chukundah'),
          'created_at' => $now,
          'updated_at' => $now
        ],
        [
          'email' => 'me@nowhere.com',
          'first_name' => 'Anon',
          'last_name' => 'User',
          'phone' => '08033339999',
          'password' => \Hash::make("anonuser"),
          'created_at' => $now,
          'updated_at' => $now
        ]
      ];

      foreach ($users as $key => $user) {
        DB::table($this->table)->insert($user);
      }
      Model::reguard();
    }
}
