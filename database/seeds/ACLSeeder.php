<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Role;
use App\Permission;
use App\User;

class ACLSeeder extends Seeder {
  private $roles_table = 'roles';
  private $perms_table = 'permissions';
  private $perm_role_table = 'permission_role';
  private $role_user_table = 'role_user';
  private $users_table = 'users';

  private $roles = [
    ['name' => 'SuperAdmin', 'display_name' => 'Super Admin', 'description' => 'Super Admin'],
    ['name' => 'Admin', 'display_name' => 'Admin', 'description' => 'Admin'],
    ['name' => 'User', 'display_name' => 'User', 'description' => 'User']
  ];
  private $perms = [
    [
      'name' => 'create-users',
      'display_name' => 'create-users',
      'description' => 'Create users'
    ],
    [
      'name' => 'update-users',
      'display_name' => 'update-users',
      'description' => 'Update users'
    ],
    [
      'name' => 'delete-users',
      'display_name' => 'delete-users',
      'description' => 'Delete users'
    ],
    [
      'name' => 'view-users',
      'display_name' => 'view-users',
      'description' => 'View users'
    ],
    [
      'name' => 'view-self',
      'display_name' => 'view-self',
      'description' => 'View own details'
    ],
    [
      'name' => 'update-self',
      'display_name' => 'update-self',
      'description' => 'Update own details'
    ],
    [
      'name' => 'delete-self',
      'display_name' => 'delete-self',
      'description' => 'Delete own account'
    ],
    [
      'name' => 'create-role',
      'display_name' => 'create-role',
      'description' => 'Create role'
    ],
    [
      'name' => 'create-permission',
      'display_name' => 'create-permission',
      'description' => 'Create permission'
    ],
    [
      'name' => 'assign-role',
      'display_name' => 'assign-role',
      'description' => 'Assign role to user'
    ],
    [
      'name' => 'attach-permission',
      'display_name' => 'attach-permission',
      'description' => 'Attach permission to role'
    ]
  ];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
      Model::unguard();
      $this->insert_roles();
      $this->insert_perms();
      $this->attach_permissions();
      $this->assign_roles();
      Model::reguard();
    }

  private function insert_roles() {
    DB::table($this->roles_table)->delete();
    foreach ($this->roles as $key => $value) {
      DB::table($this->roles_table)->insert($value);
    }
  }

  private function insert_perms() {
    DB::table($this->perms_table)->delete();
    foreach ($this->perms as $key => $value) {
      DB::table($this->perms_table)->insert($value);
    }
  }

  private function attach_permissions() {
    DB::table($this->perm_role_table)->delete();

    $role = Role::where('name', 'Admin')->first();
    $permissions = Permission::where('name', 'like', '%-users')
      ->orWhere('name', 'view-self')
      ->orWhere('name', 'like', '%-role')
      ->orWhere('name', 'like', '%-permission')
      ->get();
    $role->attachPermissions($permissions);

    $role = Role::where('name', 'User')->first();
    $permissions = Permission::where('name', 'view-self')
      ->orWhere('name', 'update-self')
      ->get();
    $role->attachPermissions($permissions);
  }

  private function assign_roles() {
    DB::table($this->role_user_table)->delete();
    $users = User::all();

    foreach ($users as $key => $user) {
      if ($user->first_name == 'Uk') {
        $user->roles()->attach(Role::where('name', 'SuperAdmin')->first()->id);
      } else if ($user->first_name == 'Popsana') {
        $user->roles()->attach(Role::where('name', 'Admin')->first()->id);
      } else {
        $user->roles()->attach(Role::where('name', 'User')->first()->id);
      }
    }
  }
}
