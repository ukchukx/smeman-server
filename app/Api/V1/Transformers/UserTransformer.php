<?php
namespace App\Api\V1\Transformers;

use App\User;
use League\Fractal\TransformerAbstract;


class UserTransformer extends TransformerAbstract {
  /**
   * Transform a User into an array
   * @param User $user
   * @return array
   */
   public function transform(User $user) {
     return [
       'id' => $user->id,
       'email' => $user->email,
       'first_name' => $user->first_name,
       'other_name' => $user->other_name,
       'last_name' => $user->last_name,
       'phone' => $user->phone,
       'user_type' => $user->user_type,
       'created_at' => $user->created_at->__toString(),
       'updated_at' => $user->updated_at->__toString()
     ];
   }
}
