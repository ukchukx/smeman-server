<?php
namespace Tests\App\Api\V1\Controllers;

use TestCase;
use App\User;
use App\Role;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Carbon\Carbon;

class AuthControllerTest extends TestCase {
  use DatabaseMigrations;

  public function setUp() {
    parent::setUp();

    $this->auth_endpoint = '/api/auth';
    $this->users_db = 'users';
  }

  /** @test **/
  public function signupShouldCreateUser() {
    $user = factory('App\User')->make();
    $user_array = $user->toArray();
    $user_array['password'] = $user->password;
    $token_release = env('API_SIGNUP_TOKEN_RELEASE', false);

    $this->post($this->auth_endpoint.'/signup', $user_array);
    if ($token_release) $this->seeStatusCode(200);
    else $this->seeStatusCode(201);
  }

  /** @test **/
  public function loginShouldReturnToken() {
    $user_array = ['email' => 'a@b.com', 'password' => 'testtest'];
    $user = factory('App\User')->create($user_array);

    $this->post($this->auth_endpoint.'/login', $user_array);
    $this->seeStatusCode(200);

    $data = json_decode($this->response->getContent(), true);
    $this->assertArrayHasKey('token', $data);
  }

  /** @test **/
  public function refreshShouldReturnToken() {
    $user_array = ['email' => 'a@b.com', 'password' => 'testtest'];
    $user = factory('App\User')->create($user_array);

    $this->post($this->auth_endpoint.'/login', $user_array);
    $this->seeStatusCode(200);

    $data = json_decode($this->response->getContent(), true);
    $this->assertArrayHasKey('token', $data);
    $user_array['token'] = $data['token'];

    $this->post($this->auth_endpoint.'/refresh', $user_array,
      ['HTTP_Authorization' => 'Bearer '.$data['token']]);
    $this->seeStatusCode(200);
    $data = json_decode($this->response->getContent(), true);
    $this->assertArrayHasKey('token', $data);
  }

  /** @test **/
  public function recoveryShouldReturn201ForValidUsers() {
    $user = factory('App\User')->create();

    $this->post($this->auth_endpoint.'/recovery', $user->toArray());
    $this->seeStatusCode(200);

    $data = json_decode($this->response->getContent(), true);
    $this->assertArrayHasKey('token', $data);
  }

  /** @test **/
  public function recoveryShouldReturn404ForInvalidUsers() {
    $user_array = ['email' => 'a@b.com'];
    $user = factory('App\User')->create($user_array);

    $this->post($this->auth_endpoint.'/recovery', ['email' => 'a1@b.com']);
    $this->seeStatusCode(404);
  }

  /** @test **/
  public function resetShouldResetPassword() {
    $user = factory('App\User')->create();
    $this->post($this->auth_endpoint.'/recovery', $user->toArray());

    $data = json_decode($this->response->getContent(), true);

    $this->post($this->auth_endpoint.'/recovery', [
      'token' => $data['token'],
      'email' => $user->email,
      'password' => 'newpassword',
      'password_confirmation' => 'newpassword'
    ])->seeStatusCode(200);

  }

}
